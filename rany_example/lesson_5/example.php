<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample of Creating User Defined Function</title>
</head>
<body>

    <?php

       function writeMsg(){
         echo "Hello World !";
     }

    function writeMessage(){
    return "Hi, Everyone !";
    }

     writeMsg(); //មិន​ប្រើ​ echo ​ទេ

    // echo writeMessage();

     ?>

    <h1>What are you saying <?php writeMsg(); ?></h1>
    
     <h2>តើអ្នក​បាន​និយាយអ្វីជាមួយមិត្តខ្ញុំ​ <span style="color: green;"><?php echo writeMessage(); ?></span></h2>
</body>
</html>
