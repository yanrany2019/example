<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>example Operator php7</title>
</head>
<body>
<h2>using Operator</h2>
     <?php
     // Increment/Decrement Operators

    $a = 5;
    $a = $a + 1;

     var_dump( $a );
     var_dump( $a++ );
     var_dump( $a );
     var_dump( ++$a );

    var_dump( $a-- );
    var_dump( $a );
    var_dump( --$a );

     echo "<div>" . $a++ . " " . $a . " " . ++$a . " " . $a . "</div>";

    ?> 
</body>
</html>