<?php
      trait ManageFruit{
        //   Properties
            protected $name;
            protected $price;

        public function __construct($defualt_name,$default_price){
            $this->name=$defualt_name;
            $this->price=$default_price;
        }

        // Methodes  or Functions      
        function set_Data($name,$price){
            $this->name=$name;
            $this->price=$price;
        }

        function get_Data(){
            return $this->name ." : ". $this->price;
        }

      }

      class Fruit{
          use ManageFruit;


          function myFruit(){
              echo "I love fruit !";
          }
      }
        
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link href="css/style.css" rel="stylesheet"> -->
    </head>
    <body>
        <h3>Using OOP with Fruit of Trait</h3>

                
     <h2>
        <?php 
            $objFruit1=new Fruit("ស្វាយ",3);
            echo $objFruit1->get_Data();      
        ?>
     </h2>

     <h2>
        <?php 

        //Assessing Methods in class
        $objFruit1->set_Data("ម្នាស់ទំ",4);
        echo $objFruit1->get_Data();      
        ?>
     </h2>


     <h2>
        <?php 

        //Assessing Methods in class
        $objFruit1->set_Data("ចេកអំបូង",9);
        echo $objFruit1->get_Data();      
        ?>
     </h2>


     <h2>
        <?php 

            $objFruit1->myFruit();     
        ?>
     </h2>

    </body>
</html>