<?php
      Class fruit{
          // Properties
          protected $name;
          protected $price;

          // Methodes
          function set_Data($name,$price){
              $this->name=$name;
              $this->price=$price;
          }
          function get_Data(){
              return $this->name ." : ". $this->price;
          }

      }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>example</title>
</head>
<body>
    <h3> Using OOP</h3>

    <?php
    //Creating Objicts
    $ObjFruit1=new fruit();

    //Assessing Methods in class
    $ObjFruit1->set_Data("banana",2);

    //$obj2=new fruit();
    //$obj2=name="ស្វាយ";
    //$obj2->price=3;

    ?>

     <h2><?php echo $ObjFruit1->get_Data(); ?></h2>
</body>
</html>