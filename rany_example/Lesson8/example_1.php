<?php

use MathStudent as GlobalMathStudent;

class Student{
        // Data Members or Properties
        protected $first_name;
        protected $last_name;

        // Create contstructor
        public function __construct($first_name,$last_name)
        {
            $this->first_name=$first_name;
            $this->last_name=$last_name;
            
        }

        public function SayHello(){
            return $this->first_name. " " . $this->last_name;
        }

    }


    class MathStudent extends Student{

        public function sum_number($first_number,$second_number){
            $sum=$first_number + $second_number;

            return $this->first_name. " " . $this->last_name ." and Sum of number is:". $sum;
        }
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>My First Bootstrap Page</h1>
  <p>Resize this responsive page to see the effect!</p> 
</div>
  
<div class="container">
  <div class="row">
    
    <div class="col-sm-6">
      <h3 class="text-success">Student Register</h3>
      <form action="L08_Ex_01.php" method="GET">
            <div class="form-group">
                <label for="first_name">First Name:</label>
                <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" id="first_name">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" name="last_name" class="form-control" placeholder="Enter password" id="last_name">
            </div>
            
            <hr>

            <div class="form-group">
                <label for="first_number">First Number:</label>
                <input type="number" name="first_number" class="form-control" placeholder="Enter password" id="first_number">
            </div>

            <div class="form-group">
                <label for="second_number">Second Number:</label>
                <input type="number" name="second_number" class="form-control" placeholder="Enter password" id="second_number">
            </div>
           
            <button type="submit" class="btn btn-primary">Submit</button>
      </form>

    </div>

    <div class="col-sm-6">
      <h3 class="text-center text-primary">Result of Submit</h3>
      
      <?php 
        @$objStu=new GlobalMathStudent($_GET['first_name'],$_GET['last_name']);
      
      ?>

    <h1><?php echo @$objStu->SayHello(); ?></h1>
    <h1><?php echo @$objStu->sum_number($_GET['first_number'],$_GET['second_number']); ?></h1>
     


    </div>

  

  </div>
</div>

</body>
</html>
